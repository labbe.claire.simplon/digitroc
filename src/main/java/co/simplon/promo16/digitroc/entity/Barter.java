package co.simplon.promo16.digitroc.entity;

import java.sql.Timestamp;

public class Barter {
    
    private Integer id;
    private Boolean status = null;
    private String message;
    private Timestamp date;
    private Dvd demandedDvd;
    private Dvd selectedDvd;
    private User demandUser;


    public Barter(String message, Dvd demandedDvd, User demandUser) {
        this.message = message;
        this.demandedDvd = demandedDvd;
        this.demandUser = demandUser;
    }

    public Barter(Integer id, Boolean status, String message, Timestamp date) {
        this.id = id;
        this.status = status;
        this.message = message;
        this.date = date;
    }

    public Barter() {
    }

    public Barter(Boolean status, String message, Timestamp date, Dvd demandedDvd, Dvd selectedDvd, User demandUser) {
        this.status = status;
        this.message = message;
        this.date = date;
        this.demandedDvd = demandedDvd;
        this.selectedDvd = selectedDvd;
        this.demandUser = demandUser;
    }

    public Barter(Integer id, Boolean status, String message, Timestamp date, Dvd demandedDvd, Dvd selectedDvd,
            User demandUser) {
        this.id = id;
        this.status = status;
        this.message = message;
        this.date = date;
        this.demandedDvd = demandedDvd;
        this.selectedDvd = selectedDvd;
        this.demandUser = demandUser;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Dvd getDemandedDvd() {
        return demandedDvd;
    }

    public void setDemandedDvd(Dvd demandedDvd) {
        this.demandedDvd = demandedDvd;
    }

    public Dvd getSelectedDvd() {
        return selectedDvd;
    }

    public void setSelectedDvd(Dvd selectedDvd) {
        this.selectedDvd = selectedDvd;
    }

    public User getDemandUser() {
        return demandUser;
    }

    public void setDemandUser(User demandUser) {
        this.demandUser = demandUser;
    }

    

    
}
