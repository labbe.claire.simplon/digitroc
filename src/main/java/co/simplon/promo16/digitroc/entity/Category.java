package co.simplon.promo16.digitroc.entity;

public class Category {
    
    private Integer id;
    private String name;
    
    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }
    
    public Category(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    

    
}
