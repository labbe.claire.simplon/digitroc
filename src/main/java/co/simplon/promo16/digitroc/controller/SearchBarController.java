package co.simplon.promo16.digitroc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo16.digitroc.entity.Dvd;
import co.simplon.promo16.digitroc.repository.DvdRepository;

@RestController
@RequestMapping("/api/dvd")
public class SearchBarController {

    @Autowired
    private DvdRepository repo;

    @GetMapping
    public List<Dvd> listDvds(@RequestParam(required = false) String search) {

        if (search != null) {
            return repo.findByTitle(search);
        }
        return repo.findAll();
    }

}
