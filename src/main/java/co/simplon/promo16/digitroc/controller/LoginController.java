package co.simplon.promo16.digitroc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.digitroc.entity.User;

@Controller
public class LoginController {

   
    @GetMapping("/login")
    public String showLogin(Model model) {
        model.addAttribute("user", new User());
        
        return "login";
    }

}
