package co.simplon.promo16.digitroc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import co.simplon.promo16.digitroc.entity.Dvd;
import co.simplon.promo16.digitroc.repository.DvdRepository;
import co.simplon.promo16.digitroc.repository.UserRepository;

@Controller
public class DvdController {

    @Autowired
    private DvdRepository repo;

    @Autowired
    private UserRepository urepo;

    @GetMapping("dvd/{id}")
    public String showOneDvd(@PathVariable int id, Model model) {
        Dvd dvd = repo.findById(id);
        model.addAttribute("dvd", dvd);
        return "dvd";
    }

    @GetMapping("/library")
    public String showAllDvd(Model model) {
        model.addAttribute("dvds", repo.findAll());
        model.addAttribute("users", urepo.findAll());
        return "library";
    }

    @GetMapping("/user-library/{id}")
    public String showAllDvdByUser(Model model, @PathVariable Integer id) {
        model.addAttribute("dvdo", repo.findByOwner(id));
        model.addAttribute("user", urepo.findById(id));
        return "user-library";
    }

    @GetMapping("/")
    public String showLastDvd(Model model) {
        model.addAttribute("dvds", repo.findByDate().subList(0, 5));
        return "index";
    }

    @GetMapping("/search")
    public String showDvdByKeyWord(Model model, String keyword) {
        
        model.addAttribute("dvdlist", repo.findByKeyword(keyword));

        return "search";
    }

    // @PostMapping("/dvd/{id}")
    // public String deleteDvd (Dvd dvd) {
    // repo.deleteById(dvd);;
    // return "profil";
    // }

}
