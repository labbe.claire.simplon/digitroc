package co.simplon.promo16.digitroc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import co.simplon.promo16.digitroc.entity.User;
import co.simplon.promo16.digitroc.repository.BarterRepository;
import co.simplon.promo16.digitroc.repository.DvdRepository;

@Controller
public class BarterController {
    
    @Autowired
    private BarterRepository repo;

    @Autowired
    private DvdRepository dvdrepo;

    @GetMapping("/barter")
    public String showAllBarterByUser(Model model, Authentication authentication) {
        User actualUser = (User)authentication.getPrincipal();
        model.addAttribute("barters", repo.findByOwner(actualUser.getId()));
        return "barter";        
    }

    @GetMapping("/barter-notif")
    private String showBarterResponse(Model model, Authentication authentication, @PathVariable Integer id) {
        User actualUser = (User)authentication.getPrincipal();
        model.addAttribute(repo.findById(id));
        model.addAttribute("actualUser", actualUser);
        model.addAttribute("dvd", dvdrepo.findByOwner(id));
        return "barter-notif";
    }
    // @PostMapping("/return-barter")
    // public String processAddResponse(Barter barter, @RequestParam("file") MultipartFile file, Authentication authentication, User user) {
    //     User actualUser = (User)authentication.getPrincipal();
        

    //     return "redirect:/";

    // }
}
