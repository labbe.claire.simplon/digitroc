package co.simplon.promo16.digitroc.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.digitroc.entity.Category;

@Repository
public class CategoryRepository {
    private static final String GET_CATEGORY_LIST = "SELECT * FROM category";
    private static final String GET_CATEGORY_BY_ID = "SELECT * FROM category where id = ? ";
    private static final String ADD_CATEGORY_TO_DVD = "INSERT INTO dvd (id, category_id) VALUES (?,?)";
    private static final String GET_CATEGORY_BY_DVD = "SELECT * FROM dvd where category_id = ? ";

    @Autowired
    DataSource dataSource;

    Connection cnx;

    public List<Category> findAll() {
        List<Category> categoryList = new ArrayList<>();
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(GET_CATEGORY_LIST);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Category category = new Category(
                        rs.getInt("id"),
                        rs.getString("name"));
                categoryList.add(category);
            }
            cnx.close();
            return categoryList;
        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            // On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou
            // non
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return null;
    }

    public Category findById(Integer id) {
        Category category = null;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_CATEGORY_BY_ID);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                category = new Category(
                        rs.getInt("id"),
                        rs.getString("name"));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return category;
    }

    public List<Category> findCategoryByDvd (Integer id) {
        List<Category> catList = new ArrayList<>() ;
        try {
            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_CATEGORY_BY_DVD);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                catList.add(new Category(
                        rs.getInt("id"),
                        rs.getString("name")));
            }

        } catch (SQLException e) {

            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
        return catList;
    }

    public void addCategoryToDvd(Integer dvdId, Integer categoryId) {
        try {
            cnx = dataSource.getConnection();
            PreparedStatement stmt = cnx.prepareStatement(ADD_CATEGORY_TO_DVD);
            stmt.setInt(1, dvdId);
            stmt.setInt(2, categoryId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            
            e.printStackTrace();
        }
        finally {
            //On ferme la connection dans la finally qui sera exécuté qu'on ait catché ou non
            DataSourceUtils.releaseConnection(cnx, dataSource);
        }
    }



}
