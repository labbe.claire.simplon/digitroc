package co.simplon.promo16.digitroc.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import co.simplon.promo16.digitroc.entity.User;


@ActiveProfiles("test")
@SpringBootTest
@Sql(scripts={"/databasetest.sql"}, executionPhase = ExecutionPhase.BEFORE_TEST_METHOD)
public class UserRepositoryTest {

    @Autowired
    UserRepository repo;


    @Test
    void testFindAll() {
        List<User> result = repo.findAll();
        assertEquals(1, result.size());
        User first = result.get(0);
        assertEquals("test@test.com", first.getEmail());
        assertEquals("ROLE_USER", first.getRole());
        
    }

    @Test
    void testFindByEmail() {
        User first = repo.findByEmail("test@test.com");
        assertEquals("test@test.com", first.getEmail());
        assertEquals("ROLE_USER", first.getRole());

    }
    
    @Test
    void testFindByEmailUserNotFound() {
        User first = repo.findByEmail("blablabla");
        assertNull(first);

    }

    @Test
    void testLoadUserByUsernameShouldThrowOnUserNotFound() {
        //Test qu'une UsernameNotFoundException sera levée lorsqu'on exécutera le
        //repo.loadUserByUsername("blabla")
        assertThrows(UsernameNotFoundException.class, () -> {
            repo.loadUserByUsername("blabla");
        });
        
    }

    @Test
    void testSave() {

        User toSave = new User("TestUser", "test.user@test.com", "1234", "Cédric", "Exbrayat","Lyon");
        User savedUser = repo.save(toSave);
        assertNotNull(savedUser.getId());
        List<User> result = repo.findAll();
        assertEquals(2, result.size());
    }
    
    @Test
    void testUpdate() {
        User toUpdate = new User(1, "TestUser", "test.user@test.com", "1234", "Hélyette", "Haon","Lyon");
        User updateUser = repo.update(toUpdate);
        assertEquals(1, updateUser.getId());
        assertEquals("Hélyette", updateUser.getFirstname());
        
    }

}
